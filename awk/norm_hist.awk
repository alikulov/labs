BEGIN {
	print START
	print 
}
{
if ($1 < 10) 
numbers["0-9"] += 1
if ($1 > 9 && $1 < 20)
numbers["10-19"] += 1
if ($1 > 19 && $1 < 30)
numbers["20-29"] += 1
if ($1 > 29 && $1 < 40)
numbers["30-39"] += 1
if ($1 > 39 && $1 < 50)
numbers["40-49"] += 1
if ($1 > 49 && $1 < 60)
numbers["50-59"] += 1
if ($1 > 59 && $1 < 70)
numbers["60-69"] += 1
if ($1 > 69 && $1 < 80)
numbers["70-79"] += 1
if ($1 > 79 && $1 < 90)
numbers["80-89"] += 1
if ($1 > 89 && $1 < 100)
numbers["90-99"] += 1
if ($1 == 100)
numbers["100"] += 1
}
END {
	for (num in numbers) {
	printf num
	perc = numbers[num]*100/199
	printf (": %i ", perc)
	for (i=0; i<=perc; i++)
	{
		printf "*" 
	}
	printf "\n" 
	}
	
}

