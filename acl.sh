#!/bin/bash
mkdir Projects
chmod 777 Projects
cd Projects
mkdir Proj1 && chmod +t Proj1
mkdir Proj2 && chmod +t Proj2
mkdir Proj3 && chmod +t Proj3
sudo addgroup devs
sudo addgroup infomanagers
sudo addgroup analysts

for i in `seq 1 5`;
do
	sudo useradd  -g devs  -s /bin/bash -p $(openssl passwd -1 $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)) R$i
done  

for i in `seq 1 3`;
do
	sudo useradd  -g infomanagers  -s /bin/bash -p $(openssl passwd -1 $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)) I$i
done  

for i in `seq 1 4`;
do
	sudo useradd  -g analysts  -s /bin/bash -p $(openssl passwd -1 $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)) A$i
done  

#setfacl -dm "u::rwx,g::rwx,o::r-x" Proj1
#setfacl -dm "u::rwx,g::rwx,o::r-x" Proj2
#setfacl -dm "u::rwx,g::rwx,o::r-x" Proj3
setfacl -m "u:R2:rwx,u:R3:rwx,u:R5:rwx,u:A1:rwx,u:A4:r" Proj1
setfacl -m "u:R1:rwx,u:R5:rwx,u:A1:rwx,u:A2:r,u:A3:r" Proj2
setfacl -m "u:R1:rwx,u:R2:rwx,u:R4:rwx,u:A2:rwx,u:A1:r,u:A4:r" Proj3
setfacl -m "g:infomanagers:rwx" Proj1
setfacl -m "g:infomanagers:rwx" Proj2
setfacl -m "g:infomanagers:rwx" Proj3

setfacl -dm "u:R2:rwx,u:R3:rwx,u:R5:rwx,u:A1:rwx,u:A4:r" Proj1
setfacl -dm "u:R1:rwx,u:R5:rwx,u:A1:rwx,u:A2:r,u:A3:r" Proj2
setfacl -dm "u:R1:rwx,u:R2:rwx,u:R4:rwx,u:A2:rwx,u:A1:r,u:A4:r" Proj3
setfacl -dm "g:infomanagers:rwx" Proj1
setfacl -dm "g:infomanagers:rwx" Proj2
setfacl -dm "g:infomanagers:rwx" Proj3



